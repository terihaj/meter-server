require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const logger = require('morgan');
const users = require('./routes/users');
const meters = require('./routes/meters');
const readings = require('./routes/readings');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { getUserByName, createUser } = require('./database');

app.use(cors());

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1] // TODO
  if (token == null)
    return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err)
      res.sendStatus(403);
    
    req.user = user;
    next();
  })
}

app.use(express.json());
app.use(logger('[:date[web]] :remote-addr :method :url :status :response-time ms'));
app.use('/users', authenticateToken, users);
app.use('/meters', authenticateToken, meters);
app.use('/readings', readings);


app.post('/login', async (req, res) => {
  const { name, password } = req.body;
  if (!name || !password) return res.sendStatus(400);

  try {
    const userData = await getUserByName(name);

    if (!await bcrypt.compare(password, userData.password))
      return res.sendStatus(401); // wrong password
    
    const user = {
      id: userData.id,
      name: userData.name
    };

    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
    res.json({ accessToken, user });
  }
  catch (err) {
    console.log('error getting user:', err);
    // no user with name specified or
    // other internal error
    return res.sendStatus(err.received === 0 ? 404 : 500);
  }
});

app.post('/register', async (req, res) => {
  const { name, password } = req.body;
  if (!name || !password) return res.sendStatus(400);

  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    const userData = await createUser(name, hashedPassword);

    const user = {
      id: userData.id,
      name: userData.name
    };

    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
    res.json({ accessToken, user });
  }
  catch (err) {
    if (err.code == 23505) {
      return res.sendStatus(409); // user already exists
    }
    
    return res.sendStatus(500); // other internal error
  }
});


// error handler
app.use((err, req, res, next) => {
  console.log(err);

  if (err) {
    let code;
    switch (err.code) {
      case 0:
        code = 404;
        break;
      case '23505': // unique constraint
        code = 409;
        break;
      case '22P02': // malformed value
        code = 400;
        break;
      default:
        code = 500;
        break;
    }
    res.sendStatus(code);
  }
  else {
    res.sendStatus(500);
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`);
});