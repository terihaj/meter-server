const express = require('express');
const router = express.Router();
const { db } = require('../database');

router.param('readingId', (req, res, next, readingId) => {
  if (readingId) next();
  else res.status(400).send('Missing readingId');
});

router.get('/', async (req, res, next) => {
  try {
    let readings = await db.any('SELECT * FROM "meter_reading"');
    res.json(readings);
  }
  catch (err) { next(err); }
})

router.post('/', async (req, res, next) => {
  try {
    const { key, value, duration } = req.body;
    if (!key || value === undefined || duration === undefined) return res.sendStatus(400);

    let reading = await db.one(`
      INSERT INTO "meter_reading" ("meter_id", "value", "ended_at", "duration")
      SELECT "meter"."id", $1, CURRENT_TIMESTAMP, $2
      FROM "meter" WHERE "meter"."key" = $3
      RETURNING *`, [value, duration, key]);
    res.json(reading);
  }
  catch (err) { next(err); }
});

router.get('/:readingId', async (req, res, next) => {
  try {
    let reading = await db.one('SELECT * FROM "meter_reading" WHERE "id" = $1', req.params.readingId);
    res.json(reading);
  }
  catch (err) { next(err); }
});

module.exports = router;
