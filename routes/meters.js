const express = require('express');
const router = express.Router();
const { db } = require('../database');

router.param('meterId', (req, res, next, meterId) => {
  if (meterId) next();
  else res.status(400).send('Missing meterId');
});

const groupByValues = ['minute', 'hour', 'day', 'week', 'month', 'year'];

router.param('groupBy', (req, res, next, groupBy) => {
    if (groupByValues.includes(groupBy)) next();
    else res.sendStatus(404);
});

router.get('/', async (req, res, next) => {
  try {
    let meters = await db.any('SELECT "id", "name", "last_active_at" FROM "meter"');
    res.json(meters);
  }
  catch (err) { next(err); }
});

router.post('/', async (req, res, next) => {
  try {
    const { name, key } = req.body;
    if (!name || !key) return res.sendStatus(400);

    let meter = await db.one('INSERT INTO "meter" ("name", "key") VALUES ($1, $2) RETURNING "id"', [name, key]);
    res.json(meter);
  }
  catch (err) { next(err); }
});

router.get('/:meterId', async (req, res, next) => {
  try {
    let meter = await db.one('SELECT "id", "name", "last_active_at" FROM "meter" WHERE "id" = $1', req.params.meterId);
    res.json(meter);
  }
  catch (err) { next(err); }
});

router.put('/:meterId', async (req, res, next) => {
  try {
    const { name, key } = req.body;
    if (!name || !key) return res.sendStatus(400);

    let meter = await db.one('UPDATE "meter" SET "name" = $1, "key" = $2 WHERE "id" = $3 RETURNING "id"', [name, key, req.params.meterId]);
    res.json(meter);
  }
  catch (err) { next(err); }
});

router.delete('/:meterId', async (req, res, next) => {
  try {
    let meter = await db.one('DELETE FROM "meter" WHERE "id" = $1 RETURNING "id"', req.params.meterId);
    res.json(meter);
  }
  catch (err) { next(err); }
});

router.get('/:meterId/readings', async (req, res, next) => {
  try {
    let readings = await db.any(`
      SELECT *
      FROM "meter_reading"
      WHERE "meter_id" = $1`, req.params.meterId);

    res.json(readings);
  }
  catch (err) { next(err); }
});

router.get('/:meterId/readings/:groupBy', async (req, res, next) => {
  try {
    let where = ['"meter_id" = $2'];

    if (req.query.start)
      where.push('"ended_at" >= $3');
    
    if (req.query.end)
      where.push('"ended_at" <= $4');
    
    where = where.join(' AND ');

    let query = `
      SELECT
        sum("value") AS "value",
        extract(epoch from date_trunc($1, ended_at)) * 1000 AS "timestamp"
      FROM "meter_reading"
      WHERE ${where}
      GROUP BY "timestamp"
      ORDER BY "timestamp"`;
    
    if (req.query.limit)
      query += '\nLIMIT $5';
    
    if (req.query.offset)
      query += '\nOFFSET $6';

    let params = [
      req.params.groupBy,
      req.params.meterId,
      req.query.start,
      req.query.end,
      req.query.limit,
      req.query.offset
    ];

    let readings = await db.any(query, params);

    res.json(readings);
  }
  catch (err) { next(err); }
});

module.exports = router;