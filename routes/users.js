const express = require('express');
const router = express.Router();
const { db } = require('../database');

router.param('userId', (req, res, next, userId) => {
  if (userId) next();
  else res.status(400).send('Missing userId');
});

router.param('meterId', (req, res, next, meterId) => {
  if (meterId) next();
  else res.status(400).send('Missing meterId');
});

router.get('/', async (req, res, next) => {
  try {
    let users = await db.any('SELECT "id", "name" FROM "user"');
    res.json(users);
  }
  catch (err) { next(err); }
});

router.post('/', async (req, res, next) => {
  try {
    const { name, password } = req.body;
    if (!name || !password) return res.sendStatus(400);

    let user = await db.one('INSERT INTO "user" ("name", "password") VALUES ($1, $2) RETURNING "id"', [name, password]);
    res.json(user);
  }
  catch (err) { next(err); }
});

router.get('/:userId', async (req, res, next) => {
  try {
    let user = await db.one('SELECT "id", "name" FROM "user" WHERE "id" = $1', req.params.userId);
    res.json(user);
  }
  catch (err) { next(err); }
});

router.put('/:userId', async (req, res, next) => {
  try {
    const { name, password } = req.body;
    if (!name || !password) return res.sendStatus(400);

    let user = await db.one('UPDATE "user" SET "name" = $1, "password" = $2 WHERE "id" = $3 RETURNING "id"', [name, password, req.params.userId]);
    res.json(user);
  }
  catch (err) { next(err); }
});

router.delete('/:userId', async (req, res, next) => {
  try {
    let user = await db.one('DELETE FROM "user" WHERE "id" = $1 RETURNING "id"', req.params.userId);
    res.json(user);
  }
  catch (err) { next(err); }
});

router.get('/:userId/meters', async (req, res, next) => {
  try {
    let meters = await db.any(`
        SELECT "meter"."id", "meter"."name", "meter"."last_active_at"
        FROM "meter" JOIN "meter_access"
          ON "meter"."id" = "meter_access"."meter_id"
        WHERE "meter_access"."user_id" = $1`, req.params.userId);

    res.json(meters);
  }
  catch (err) { next(err); }
});

router.post('/:userId/meters', async (req, res, next) => {
  const { name, key } = req.body;
  if (!name || !key) return res.sendStatus(400);

  // exisiting meter
  try {
    let meter = await db.one('SELECT * FROM "meter" WHERE "name" = $1', name);
    if (meter.key === key) {
      try {
        await db.one('INSERT INTO "meter_access" ("user_id", "meter_id") VALUES ($1, $2) RETURNING *', [req.params.userId, meter.id]);
      }
      catch(err2) {
        return res.status(400).json({error: {name: 'You already have access to this meter'}});
      }
      return res.json({id: meter.id});
    }
    return res.status(400).json({error: {key: 'Invalid key'}});
  }
  catch (err) {
    if (err.code !== 0) return next(err);
  }

  // new meter
  try {
    let meter = await db.one('INSERT INTO "meter" ("name", "key") VALUES ($1, $2) RETURNING "id"', [name, key]);
    await db.one('INSERT INTO "meter_access" ("user_id", "meter_id") VALUES ($1, $2) RETURNING *', [req.params.userId, meter.id]);
  }
  catch (err) { next(err); }
});

router.delete('/:userId/meters/:meterId', async (req, res, next) => {
  const { userId, meterId } = req.params;
  try {
    await db.any('DELETE FROM "meter_access" WHERE "user_id" = $1 AND "meter_id" = $2', [userId, meterId]);
    res.sendStatus(200);
  }
  catch(err) {
    next(err);
  }
});

router.get('/:userId/meters/:meterId', async (req, res, next) => {
  try {
    let meter = await db.one(`
      SELECT "meter"."id", "meter"."name", "meter"."last_active_at"
      FROM "meter" JOIN "meter_access"
        ON "meter"."id" = "meter_access"."meter_id"
      WHERE "meter_access"."user_id" = $1
        AND "meter_access"."meter_id" = $2`, [req.params.userId, req.params.meterId]);

    res.json(meter);
  }
  catch (err) { next(err); }
});

module.exports = router;