const pgp = require('pg-promise')();
const db = pgp(process.env.DBSTRING);

function getUserByName(name) {
  return db.one('SELECT * FROM "user" WHERE "name" = $1', name);
}

function createUser(name, hashedPassword) {
  return db.one('INSERT INTO "user" ("name", "password") VALUES ($1, $2) RETURNING *', [name, hashedPassword]);
}

module.exports = { db, getUserByName, createUser };